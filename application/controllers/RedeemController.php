<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RedeemController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Redeem');
    }

    public function getRedeems($page, $size)
    {

        $response = array(
            'content' => $this->Redeem->getRedeem(($page - 1) * $size, $size)->result(),
            'totalPages' => ceil($this->Redeem->getCountRedeem() / $size));

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function saveRedeems()
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        $data_redeem['id_reward'] = $data['id_reward'];
        $data_redeem['id_user'] = $data['id_user'];
        $data_redeem['redem_key'] = time();


        $id_redeem = $this->Redeem->insertRedeem($data_redeem);

        $response = array(
          'content' => $this->Redeem->getRedeemId(($page - 1) * $size, $size,$id_redeem)->result(),
          'totalPages' => ceil($this->Redeem->getCountRedeemId($id_redeem) / $size));

        $this->output
            ->set_status_header(201)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }


}
