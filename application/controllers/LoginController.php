<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('apikeycheck');
        $this->load->helper('login');
        $this->load->model('RoleUser');
        $this->load->model('LoginModel');
    }


    public function ShowUserAdmin($page, $size) {
      $data = (array)json_decode(file_get_contents('php://input'));
      if (apiKey($this->input->get('api_key'))) {
        $response = array(
            'content' => $this->LoginModel->getdatauserAdmin(($page - 1) * $size, $size)->result(),
            'totalPages' => ceil($this->LoginModel->getCountLoginAdmin() / $size));

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Gagal Login');

          $this->output
              ->set_status_header(400)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
      }
    }
    public function loginuser()
    {
      $data = (array)json_decode(file_get_contents('php://input'));
      if (apiKey($data['api_key'])) {
          if(cek_login($data['email'], $data['password'])){

          $response = array(
            'content' => $this->LoginModel->datauser(($page - 1) * $size, $size,$data['email'])->result(),
            'totalPages' => "1");
            //'totalPages' => ceil($this->LoginModel->getCountLoginUser($data['email']) / $size));

            $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                ->_display();
            exit;
          }
          else {
            $response = array(
                'Success' => false,
                'Info' => 'Gagal Login');

              $this->output
                  ->set_status_header(400)
                  ->set_content_type('application/json', 'utf-8')
                  ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                  ->_display();
              exit;
          }
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid Api Key');

          $this->output
              ->set_status_header(400)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
      }
    }
    public function insertUser()
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        if (apiKey($data['api_key'])) {
            $idrole = $this->RoleUser->getidRole($data['nama_role']);
            $data_login['nama'] = $data['nama'];
            $data_login['email'] = $data['email'];
            $data_login['password'] = $data['password'];
            $data_login['telepon'] = $data['telepon'];
            $data_login['poin'] = $data['poin'];
            $data_login['id_role'] = $idrole;

            
            $this->LoginModel->insertuserapp($data_login);

            $response = array(
                'Success' => true,
                'Info' => 'Data Tersimpan');

            $this->output
                ->set_status_header(201)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                ->_display();
            exit;
        }
        else {
          $response = array(
              'Success' => false,
              'Info' => 'Invalid API Key');

          $this->output
              ->set_status_header(400)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
        }
    }


    public function updateUser($id_user)
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        if (apiKey($data['api_key'])) {
          $data_user['nama'] = $data['nama'];
          $data_user['email'] = $data['email'];
          $data_user['telepon'] = $data['telepon'];
          $this->LoginModel->updateUserModel($data_user, $id_user);

          $response = array(
              'Success' => true,
              'Info' => 'Data Berhasil di update');

          $this->output
              ->set_status_header(200)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid APi Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }

    public function updatePoinUser($id_user)
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        if (apiKey($data['api_key'])) {
          $data_user['poin'] = $data['poin'];
          $poinlama = $this->LoginModel->getPoin($id_user);

          $this->LoginModel->updatePoinUser($data_user, $id_user, $poinlama);

          $response = array(
              'Success' => true,
              'Info' => 'Data Berhasil di update');

          $this->output
              ->set_status_header(200)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid APi Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }

    public function updateRedeemPoinUser($id_user)
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        if (apiKey($data['api_key'])) {
          $data_user['poin'] = $data['poin'];
          $poinlama = $this->LoginModel->getPoin($id_user);

          $this->LoginModel->updateRedeemPoin($data_user, $id_user, $poinlama);

          $response = array(
              'Success' => true,
              'Info' => 'Data Berhasil di update');

          $this->output
              ->set_status_header(200)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
      }else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid APi Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }

    public function deleteUserAps($id_user)
    {
      if (apiKey($this->input->get('api_key'))) {
      $this->LoginModel->deleteUser($id_user);

        $response = array(
            'Success' => true,
            'Info' => 'Data Berhasil di hapus');

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid APi Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }

    public function showPoinUser($page,$size) {

      $data = (array)json_decode(file_get_contents('php://input'));
      $username = $this->input->get('id_user');
      if (apiKey($this->input->get('api_key'))) {
        $response = array(
            'content' => $this->LoginModel->ShowDataUser(($page - 1) * $size, $size, $username)->result(),
            'totalPages' => '1');

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;

        }
        else {
          $response = array(
              'Success' => false,
              'Info' => 'Invalid APi Key');

          $this->output
              ->set_status_header(400)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
        }
    }

}
