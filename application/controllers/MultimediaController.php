<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MultimediaController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Multimediacagar');
        $this->load->helper('apikeycheck');
    }
    public function saveMultimedia()
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        if(apiKey($data['api_key'])) {
          $data_multimedia['id_cagar'] = $data['id_cagar'];
          $data_multimedia['url'] = "img_".time().".jpg" ;
          $data_multimedia['nama_multimedia'] = $data['nama_multimedia'];
          $data_multimedia['tipe'] = $data['tipe'];
          $data_multimedia['id_user'] = $data['id_user'];
          $data_multimedia['image_base64'] = $data['image_base64'];

          $this->Multimediacagar->insertMultimedia($data_multimedia);

          $response = array(
              'Success' => true,
              'Info' => 'Data Tersimpan');

          $this->output
              ->set_status_header(201)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
        }
        else {
          $response = array(
              'Success' => false,
              'Info' => 'Invalid API Key');

          $this->output
              ->set_status_header(400)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
        }
    }
    
    public function updateCagarBudaya($id_cagar)
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        if (apiKey($data['api_key'])) {
          $this->CagarBudaya->updateCagarBudaya($data, $id_cagar);

          $response = array(
              'Success' => true,
              'Info' => 'Data Berhasil di update');

          $this->output
              ->set_status_header(200)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid API Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }

  }
