<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ChallengeController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Challenge');
    }

    public function getChallenge($page, $size)
    {

        $response = array(
            'content' => $this->Challenge->getChallenge(($page - 1) * $size, $size)->result(),
            'totalPages' => ceil($this->Challenge->getCountChallenge() / $size));

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function saveChallenge()
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        $this->Challenge->insertChallenge($data);

        $response = array(
            'Success' => true,
            'Info' => 'Data Tersimpan');

        $this->output
            ->set_status_header(201)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function updateChallenge($id_challenge)
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        $this->Challenge->updateChallenge($data, $id_challenge);

        $response = array(
            'Success' => true,
            'Info' => 'Data Berhasil di update');

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function deleteChallenge($id_challenge)
    {
        $this->Challenge->deleteChallenge($id_challenge);

        $response = array(
            'Success' => true,
            'Info' => 'Data Berhasil di hapus');

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

}