<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CagarBudayaController extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('CagarBudaya');
        $this->load->model('Multimediacagar');
        $this->load->helper('apikeycheck');
    }
    public function index(){
      $this->load->view('upload_form');
    }

    public function getCagarBudaya($page, $size)
    {
      $data = $this->input->get('status');
      $data1 = $this->input->get('id_user');
      if (apiKey($this->input->get('api_key'))) {
        if($data != "" & $data1 != ""){
          $response = array(
              'content' => $this->CagarBudaya->getCagarBudayaStatusNuser(($page - 1) * $size, $size,$data, $data1),
              'totalPages' => ceil($this->CagarBudaya->getCountCagarBudayaStatusnUser($data,$data1) / $size));
        }
        else if($data != "") {
          $response = array(
              'content' => $this->CagarBudaya->getCagarBudayaStatus(($page - 1) * $size, $size,$data),
              'totalPages' => ceil($this->CagarBudaya->getCountCagarBudayaStatus($data) / $size));
        }
        else {
          $response = array(
              'content' => $this->CagarBudaya->getCagarBudaya(($page - 1) * $size, $size),
              'totalPages' => ceil($this->CagarBudaya->getCountCagarBudaya() / $size));
        }


        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      } else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid API Key');

        $this->output
            ->set_status_header(201)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }


    public function seachCagar(){

      $data = (array)json_decode(file_get_contents('php://input'));
      $data = $this->input->get('keyword');

      $response = array(
          'content' => $this->CagarBudaya->SearchgetCagarBudaya(($page - 1) * $size, $size,$data),
          'totalPages' => ceil($this->CagarBudaya->getCountCagarBudaya() / $size));

          $this->output
              ->set_status_header(201)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
    }
    public function saveCagarBudaya()
    {

        $data = (array)json_decode(file_get_contents('php://input'));
        if (apiKey($data['api_key'])) {
            $data_cb['nama_cagar'] = $data['nama_cagar'];
            $data_cb['deskripsi'] = $data['deskripsi'];
            $data_cb['tahun_peninggalan'] = $data['tahun_peninggalan'];
            $data_cb['nama_jenis'] = $data['nama_jenis'];
            $data_cb['id_user'] = $data['id_user'];
            $data_cb['longitude'] = $data['longitude'];
            $data_cb['latitude'] = $data['latitude'];
            $data_cb['alamat'] = $data['alamat'];
            $data_cb['status'] = $data['status'];

            $this->db->trans_start();
            $id_cagar = $this->CagarBudaya->insertCagarBudaya($data_cb);
            foreach($data['multimedia'] as $data_mul):
              //$data_multimedia['url'] = $data_mul->url;
              $data_multimedia['nama_multimedia'] = $data_mul->nama_multimedia;
              $data_multimedia['tipe'] = $data_mul->tipe;
              $data_multimedia['id_user'] = $data_mul->id_user;
              $data_multimedia['id_cagar'] = $id_cagar;
              $data_multimedia['url'] = "img_".time().".jpg" ;
              $data_multimedia['image_base64'] = $data_mul->image_base64;




              $this->Multimediacagar->insertMultimedia($data_multimedia);
            endforeach;
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
              $this->db->trans_rollback();
              return 'Data Gagal';
            } else {
              $this->db->trans_commit();
              $response = array(
                  'Success' => true,
                  'Info' => 'Data Tersimpan');

              $this->output
                  ->set_status_header(201)
                  ->set_content_type('application/json', 'utf-8')
                  ->set_output(json_encode($response, JSON_PRETTY_PRINT))
                  ->_display();
              exit;
            }
      } else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid API Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }




    public function updateCagarBudaya($id_cagar)
    {
        $data = (array)json_decode(file_get_contents('php://input'));
        if (apiKey($data['api_key'])) {
          $this->CagarBudaya->updateCagarBudaya($data, $id_cagar);

          $response = array(
              'Success' => true,
              'Info' => 'Data Berhasil di update');

          $this->output
              ->set_status_header(200)
              ->set_content_type('application/json', 'utf-8')
              ->set_output(json_encode($response, JSON_PRETTY_PRINT))
              ->_display();
          exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid API Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }

    public function updateStatus($id_cagar) {

      $data = (array)json_decode(file_get_contents('php://input'));
      if (apiKey($data['api_key'])) {
        $this->CagarBudaya->updateCagarStatus($data, $id_cagar);

        $response = array(
            'Success' => true,
            'Info' => 'Data Berhasil di update');

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid API Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }
    public function deleteCagarBudaya($id_cagar)
    {
      if (apiKey($this->input->get('api_key'))) {
      $this->Multimediacagar->deleteMulByIdCagar($id_cagar);
        $this->CagarBudaya->deleteCagarBudaya($id_cagar);

        $response = array(
            'Success' => true,
            'Info' => 'Data Berhasil di hapus');

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
      else {
        $response = array(
            'Success' => false,
            'Info' => 'Invalid APi Key');

        $this->output
            ->set_status_header(400)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT))
            ->_display();
        exit;
      }
    }

}
