<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//-------------------------sipp--------------------------
$route['api/cagarbudaya/(:num)/(:num)']['GET'] = 'CagarBudayaController/getCagarBudaya/$1/$2';
$route['api/cagarbudaya']['POST'] = 'CagarBudayaController/saveCagarBudaya';
$route['api/cagarbudaya/(:any)']['PUT'] = 'CagarBudayaController/updateCagarBudaya/$1';
$route['api/cagarbudaya/updatestatus/(:any)']['PUT'] = 'CagarBudayaController/updateStatus/$1';
$route['api/cagarbudaya/(:any)']['DELETE'] = 'CagarBudayaController/deleteCagarBudaya/$1';
$route['api/cagarbudaya/search/(:num)/(:num)']['GET'] = 'CagarBudayaController/seachCagar/$1/$2';

//------------------------sippp--------------------
//$route['api/loginuser/(:num)/(:num)']['GET'] = 'LoginController/loginuser/$1/$2';
$route['api/user/(:num)/(:num)']['GET'] = 'LoginController/ShowUserAdmin/$1/$2';
$route['api/loginuser']['POST'] = 'LoginController/loginuser';
$route['api/user/signup']['POST'] = 'LoginController/insertUser';
$route['api/user/poin/(:any)']['PUT'] = 'LoginController/updatePoinUser/$1';
$route['api/user/redeempoin/(:any)']['PUT'] = 'LoginController/updateRedeemPoinUser/$1';
$route['api/user/(:any)']['PUT'] = 'LoginController/updateUser/$1';
$route['api/user/(:any)']['DELETE'] = 'LoginController/deleteUserAps/$1';
$route['api/user/showpoin/(:num)/(:num)']['GET'] = 'LoginController/showPoinUser/$1/$2';

//----------------Redeem--------------------------
$route['api/redeem/(:num)/(:num)']['GET'] = 'RedeemController/getRedeems/$1/$2';
$route['api/redeem']['POST'] = 'RedeemController/saveRedeems';
//------------------------------------------------

$route['api/challenge/(:num)/(:num)']['GET'] = 'ChallengeController/getChallenge/$1/$2';
$route['api/challenge']['POST'] = 'ChallengeController/saveChallenge';
$route['api/challenge/(:any)']['PUT'] = 'ChallengeController/updateChallenge/$1';
$route['api/challenge/(:any)']['DELETE'] = 'ChallengeController/deleteChallenge/$1';

$route['api/rewards/(:num)/(:num)']['GET'] = 'RewardsController/getRewards/$1/$2';
$route['api/rewards']['POST'] = 'RewardsController/saveRewards';
$route['api/rewards/(:any)']['PUT'] = 'RewardsController/updateRewards/$1';
$route['api/rewards/(:any)']['DELETE'] = 'RewardsController/deleteRewards/$1';
