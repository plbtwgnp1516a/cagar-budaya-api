<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Multimediacagar extends CI_Model {

    public function getCountMultimedia()
    {
        return $this->db->count_all_results('challenge', FALSE);
    }

    public function insertMultimedia($dataMultimedia)
    {
        $val = array(
            'id_cagar' => $dataMultimedia['id_cagar'],
            'url' => $dataMultimedia['url'],
            'nama_multimedia' => $dataMultimedia['nama_multimedia'],
            'tipe' => $dataMultimedia['tipe'],
            'id_user' => $dataMultimedia['id_user'],
             );
             $photoname = $dataMultimedia['url'];
             $decode_img = base64_decode($dataMultimedia['image_base64']);
             
             file_put_contents('upload/files/'.$photoname, $decode_img);

        $this->db->insert('multimedia_cagar', $val);
    }

    public function updateMultimedia($dataMultimedia, $id_multimedia)
    {
        $val = array(
            'url' => $dataMultimedia['url'],
            'nama_multimedia' => $dataMultimedia['nama_multimedia'],
            'tipe' => $dataMultimedia['tipe']
        );
        $this->db->where('id_multimedia', $id_multimedia);
        $this->db->update('multimedia_cagar', $val);
    }

    public function deleteMultimedia($id_multimedia)
    {
        $val = array(
            'id_multimedia' => $id_multimedia
        );
        $this->db->delete('multimedia_cagar', $val);
    }

    public function deleteMulByIdCagar($id_cagar)
    {
        $val = array(
            'id_cagar' => $id_cagar
        );
        $this->db->delete('multimedia_cagar', $val);
    }

}
