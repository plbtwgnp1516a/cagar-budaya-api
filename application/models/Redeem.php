<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redeem extends CI_Model {

  public function getCountRedeem()
  {
      return $this->db->count_all_results('redeem', FALSE);
  }
    public function getCountRedeemId($id_redeem)
    {
        $this->db->select('*');
        $this->db->from('redeem');
        $this->db->where('id_redeem', $id_redeem);
        $data =  $this->db->count_all_results('', FALSE);
        return $data;
    }

    public function getRedeem($page, $size)
    {
        return $this->db->get('redeem', $size, $page);
    }

    public function getRedeemId($page, $size, $id_redeem)
    {
        $this->db->select('*');
        $this->db->from('redeem');
        $this->db->where('id_redeem', $id_redeem);
        $data = $this->db->get('', $size, $page);
        return $data;
    }

    public function insertRedeem($dataRedeem)
    {
        $val = array(
            'redem_key' => $dataRedeem['redem_key'],
            'id_user' => $dataRedeem['id_user'],
            'id_reward' => $dataRedeem['id_reward']
             );
        $this->db->insert('redeem', $val);
        return $this->db->insert_id();
    }

}
