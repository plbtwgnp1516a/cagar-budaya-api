<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Challenge extends CI_Model {

    public function getCountChallenge()
    {
        return $this->db->count_all_results('challenge', FALSE);
    }

    public function getChallenge($page, $size)
    {
        return $this->db->get('challenge', $size, $page);
    }

    public function insertChallenge($dataChallenge)
    {
        $val = array(
            'nama_challenge' => $dataChallenge['nama_challenge'],
            'deskripsi' => $dataChallenge['deskripsi']
             );
        $this->db->insert('challenge', $val);
    }

    public function updateChallenge($dataChallenge, $id_challenge)
    {
        $val = array(
            'nama_challenge' => $dataChallenge['nama_challenge'],
            'deskripsi' => $dataChallenge['deskripsi']
        );
        $this->db->where('id_challenge', $id_challenge);
        $this->db->update('challenge', $val);
    }

    public function deleteChallenge($id_challenge)
    {
        $val = array(
            'id_challenge' => $id_challenge
        );
        $this->db->delete('challenge', $val);
    }

}