<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginModel extends CI_Model {

    public function getCountLogin()
    {
        return $this->db->count_all_results('user_app', FALSE);
    }
    public function getCountLoginAdmin()
    {
        $this->db->select('id_user, nama, email, telepon, poin, id_role');
        $this->db->from('user_app');
        $this->db->where('id_role',1);
        $data = $this->db->count_all_results('', FALSE);
        return $data;
    }

    public function getCountLoginUser($username) {

      $this->db->select('id_user, nama, email, telepon, poin, id_role');
      $this->db->from('user_app');
      $this->db->where('email',$username);
      $data = $this->db->count_all_results('', FALSE);
      return $data;
    }
    public function getCountLoginDataUser($username) {

      $this->db->select('id_user, nama, email, telepon, poin, id_role');
      $this->db->from('user_app');
      $this->db->where('email',$username);
      $data = $this->db->count_all_results('', FALSE);
      return $data;
    }

    public function getdatauserAdmin($size, $page)
    {   $this->db->select('id_user, nama, email, telepon, poin, id_role');
        $this->db->from('user_app');
        $this->db->where('id_role',1);
        $data =  $this->db->get('', $size, $page);
        return $data;
    }


    public function getdatauser($size, $page)
    {
        return $this->db->get('user_app', $size, $page);
    }

    public function datauser($size, $page, $username) {
      $this->db->select('id_user, nama, email, telepon, poin, id_role');
      $this->db->from('user_app');
      $this->db->where('email',$username);
      $data = $this->db->get('', $size, $page);
      return $data;
    }

    public function ShowDataUser($size, $page, $username) {
      $this->db->select('id_user, nama, email, telepon, poin, id_role');
      $this->db->from('user_app');
      $this->db->where('id_user',$username);
      $data = $this->db->get('', $size, $page);
      return $data;
    }

    public function insertuserapp($userapp) {
      $val = array(
          'nama' => $userapp['nama'],
          'email' => $userapp['email'],
          'password' => $userapp['password'],
          'telepon' => $userapp['telepon'],
          'poin' => $userapp['poin'],
          'id_role' => $userapp['id_role']
           );
      $this->db->insert('user_app', $val);
    }

    public function updateUserModel($dataUser, $id_user)
    {
        $val = array(
            'nama' => $dataUser['nama'],
            'email' => $dataUser['email'],
            'telepon' => $dataUser['telepon']
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('user_app', $val);
    }

    public function updatePoinUser($dataUser, $id_user, $poinawal)
    {
        $val = array(
            'poin' => $dataUser['poin'] + $poinawal
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('user_app', $val);
    }

    public function updateRedeemPoin($dataUser, $id_user, $poinawal)
    {
        $val = array(
            'poin' => $poinawal -  $dataUser['poin']
        );
        $this->db->where('id_user', $id_user);
        $this->db->update('user_app', $val);
    }

    public function deleteUser($id_user)
    {
        $val = array(
            'id_user' => $id_user
        );
        $this->db->delete('user_app', $val);
    }

    public function getPoin($id_user) {
      $this->db->select('*');
      $this->db->from('user_app');
      $this->db->where('id_user', $id_user);
      $data = $this->db->get('');
      return $data->row('poin');
    }



}
