<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CagarBudaya extends CI_Model {

    public function getCountCagarBudaya()
    {
        return $this->db->count_all_results('cagar_budaya', FALSE);
    }
    public function getCountCagarBudayaStatus($status)
    {
      $this->db->select('*, U.id_user,U.nama, U.email, U.telepon, U.poin');
      $this->db->from('cagar_budaya');
      $this->db->join('user_app U', 'cagar_budaya.id_user = U.id_user');
      $this->db->where('status', $status);
      $data = $this->db->count_all_results('', FALSE);
      return $data;
    }
    public function getCountCagarBudayaStatusnUser($status,$id_user)
    {
      $this->db->select('*, U.id_user,U.nama, U.email, U.telepon, U.poin');
      $this->db->from('cagar_budaya');
      $this->db->join('user_app U', 'cagar_budaya.id_user = U.id_user');
      $this->db->where('status', $status);
      $this->db->where('U.id_user', $id_user);
      $data = $this->db->count_all_results('', FALSE);
      return $data;
    }

    public function getCagarBudaya($page, $size)
    {
      $cagar = [];
      $this->db->select('id_cagar, nama_cagar, deskripsi, tahun_peninggalan,
      nama_jenis, longitude, latitude, alamat, status, U.id_user,U.nama, U.email, U.telepon, U.poin');
      $this->db->from('cagar_budaya');
      $this->db->join('user_app U', 'cagar_budaya.id_user = U.id_user');
      $data = $this->db->get('', $size, $page)->result_array();
      $i = 0;
      foreach($data as $row):
        $cagar[$i] = $row;
        $this->db->select('*, U.id_user,U.nama, U.email, U.telepon, U.poin');
        $this->db->from('multimedia_cagar');
        $this->db->join('user_app U', 'multimedia_cagar.id_user = U.id_user');
        $this->db->where('id_cagar', $row['id_cagar']);
        $multimedia_data = $this->db->get()->result_array();
        if (count($multimedia_data) == 0) {
          $cagar[$i]['multimedia'][0]['id_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['nama_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['url'] = null;
          $cagar[$i]['multimedia'][0]['tipe'] = null;
          $cagar[$i]['multimedia'][0]['nama'] = null;
          $cagar[$i]['multimedia'][0]['email'] = null;
          $cagar[$i]['multimedia'][0]['telepon'] = null;
          $cagar[$i]['multimedia'][0]['poin'] = null;
        } else {
          $j = 0;
          foreach($multimedia_data as $mrow):
            $cagar[$i]['multimedia'][$j]['id_multimedia'] = $mrow['id_multimedia'];
            $cagar[$i]['multimedia'][$j]['nama_multimedia'] = $mrow['nama_multimedia'];
            $cagar[$i]['multimedia'][$j]['url'] = $mrow['url'];
            $cagar[$i]['multimedia'][$j]['tipe'] = $mrow['tipe'];
            $cagar[$i]['multimedia'][$j]['nama'] = $mrow['nama'];
            $cagar[$i]['multimedia'][$j]['email'] = $mrow['email'];
            $cagar[$i]['multimedia'][$j]['telepon'] = $mrow['telepon'];
            $cagar[$i]['multimedia'][$j]['poin'] = $mrow['poin'];
            $j++;
          endforeach;
        }
        $i++;
      endforeach;
      return $cagar;
    }

    public function getCagarBudayaStatus($page, $size, $status)
    {
      $cagar = [];
      $this->db->select('id_cagar, nama_cagar, deskripsi, tahun_peninggalan,
      nama_jenis, longitude, latitude, alamat, status, U.id_user,U.nama, U.email, U.telepon, U.poin');
      $this->db->from('cagar_budaya');
      $this->db->join('user_app U', 'cagar_budaya.id_user = U.id_user');
      $this->db->where('status', $status);
      $data = $this->db->get('', $size, $page)->result_array();
      $i = 0;
      foreach($data as $row):
        $cagar[$i] = $row;
        $this->db->select('*, U.id_user,U.nama, U.email, U.telepon, U.poin');
        $this->db->from('multimedia_cagar');
        $this->db->join('user_app U', 'multimedia_cagar.id_user = U.id_user');
        $this->db->where('id_cagar', $row['id_cagar']);
        $multimedia_data = $this->db->get()->result_array();
        if (count($multimedia_data) == 0) {
          $cagar[$i]['multimedia'][0]['id_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['nama_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['url'] = null;
          $cagar[$i]['multimedia'][0]['tipe'] = null;
          $cagar[$i]['multimedia'][0]['nama'] = null;
          $cagar[$i]['multimedia'][0]['email'] = null;
          $cagar[$i]['multimedia'][0]['telepon'] = null;
          $cagar[$i]['multimedia'][0]['poin'] = null;
        } else {
          $j = 0;
          foreach($multimedia_data as $mrow):
            $cagar[$i]['multimedia'][$j]['id_multimedia'] = $mrow['id_multimedia'];
            $cagar[$i]['multimedia'][$j]['nama_multimedia'] = $mrow['nama_multimedia'];
            $cagar[$i]['multimedia'][$j]['url'] = $mrow['url'];
            $cagar[$i]['multimedia'][$j]['tipe'] = $mrow['tipe'];
            $cagar[$i]['multimedia'][$j]['nama'] = $mrow['nama'];
            $cagar[$i]['multimedia'][$j]['email'] = $mrow['email'];
            $cagar[$i]['multimedia'][$j]['telepon'] = $mrow['telepon'];
            $cagar[$i]['multimedia'][$j]['poin'] = $mrow['poin'];
            $j++;
          endforeach;
        }
        $i++;
      endforeach;
      return $cagar;
    }

    public function getCagarBudayaStatusNuser($page, $size, $status,$id_user)
    {
      $cagar = [];
      $this->db->select('id_cagar, nama_cagar, deskripsi, tahun_peninggalan,
      nama_jenis, longitude, latitude, alamat, status, U.id_user,U.nama, U.email, U.telepon, U.poin');
      $this->db->from('cagar_budaya');
      $this->db->join('user_app U', 'cagar_budaya.id_user = U.id_user');
      $this->db->where('status', $status);
      $this->db->where('U.id_user', $id_user);
      $data = $this->db->get('', $size, $page)->result_array();
      $i = 0;
      foreach($data as $row):
        $cagar[$i] = $row;
        $this->db->select('*, U.id_user,U.nama, U.email, U.telepon, U.poin');
        $this->db->from('multimedia_cagar');
        $this->db->join('user_app U', 'multimedia_cagar.id_user = U.id_user');
        $this->db->where('id_cagar', $row['id_cagar']);
        $multimedia_data = $this->db->get()->result_array();
        if (count($multimedia_data) == 0) {
          $cagar[$i]['multimedia'][0]['id_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['nama_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['url'] = null;
          $cagar[$i]['multimedia'][0]['tipe'] = null;
          $cagar[$i]['multimedia'][0]['nama'] = null;
          $cagar[$i]['multimedia'][0]['email'] = null;
          $cagar[$i]['multimedia'][0]['telepon'] = null;
          $cagar[$i]['multimedia'][0]['poin'] = null;
        } else {
          $j = 0;
          foreach($multimedia_data as $mrow):
            $cagar[$i]['multimedia'][$j]['id_multimedia'] = $mrow['id_multimedia'];
            $cagar[$i]['multimedia'][$j]['nama_multimedia'] = $mrow['nama_multimedia'];
            $cagar[$i]['multimedia'][$j]['url'] = $mrow['url'];
            $cagar[$i]['multimedia'][$j]['tipe'] = $mrow['tipe'];
            $cagar[$i]['multimedia'][$j]['nama'] = $mrow['nama'];
            $cagar[$i]['multimedia'][$j]['email'] = $mrow['email'];
            $cagar[$i]['multimedia'][$j]['telepon'] = $mrow['telepon'];
            $cagar[$i]['multimedia'][$j]['poin'] = $mrow['poin'];
            $j++;
          endforeach;
        }
        $i++;
      endforeach;
      return $cagar;
    }

    public function SearchgetCagarBudaya($page, $size, $keyword)
    {
      $cagar = [];
      $this->db->select('id_cagar, nama_cagar, deskripsi, tahun_peninggalan,
      nama_jenis, longitude, latitude, alamat, status, U.id_user,U.nama, U.email, U.telepon, U.poin');
      $this->db->from('cagar_budaya');
      $this->db->join('user_app U', 'cagar_budaya.id_user = U.id_user');
      $this->db->like('nama', $keyword);
      $this->db->like('U.id_user', $keyword);
      $this->db->like('U.email', $keyword);
      $data = $this->db->get('', $size, $page)->result_array();
      $i = 0;
      foreach($data as $row):
        $cagar[$i] = $row;
        $this->db->select('*, U.id_user,U.nama, U.email, U.telepon, U.poin');
        $this->db->from('multimedia_cagar');
        $this->db->join('user_app U', 'multimedia_cagar.id_user = U.id_user');
        $this->db->where('id_cagar', $row['id_cagar']);
        $multimedia_data = $this->db->get()->result_array();
        if (count($multimedia_data) == 0) {
          $cagar[$i]['multimedia'][0]['id_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['nama_multimedia'] = null;
          $cagar[$i]['multimedia'][0]['url'] = null;
          $cagar[$i]['multimedia'][0]['tipe'] = null;
          $cagar[$i]['multimedia'][0]['nama'] = null;
          $cagar[$i]['multimedia'][0]['email'] = null;
          $cagar[$i]['multimedia'][0]['telepon'] = null;
          $cagar[$i]['multimedia'][0]['poin'] = null;
        } else {
          $j = 0;
          foreach($multimedia_data as $mrow):
            $cagar[$i]['multimedia'][$j]['id_multimedia'] = $mrow['id_multimedia'];
            $cagar[$i]['multimedia'][$j]['nama_multimedia'] = $mrow['nama_multimedia'];
            $cagar[$i]['multimedia'][$j]['url'] = $mrow['url'];
            $cagar[$i]['multimedia'][$j]['tipe'] = $mrow['tipe'];
            $cagar[$i]['multimedia'][$j]['nama'] = $mrow['nama'];
            $cagar[$i]['multimedia'][$j]['email'] = $mrow['email'];
            $cagar[$i]['multimedia'][$j]['telepon'] = $mrow['telepon'];
            $cagar[$i]['multimedia'][$j]['poin'] = $mrow['poin'];
            $j++;
          endforeach;
        }
        $i++;
      endforeach;
      return $cagar;
    }


    public function insertCagarBudaya($dataCagarBudaya)
    {
        $val = array(
            'nama_cagar' => $dataCagarBudaya['nama_cagar'],
            'deskripsi' => $dataCagarBudaya['deskripsi'],
            'tahun_peninggalan' => $dataCagarBudaya['tahun_peninggalan'],
            'nama_jenis' => $dataCagarBudaya['nama_jenis'],
            'id_user' => $dataCagarBudaya['id_user'],
            'longitude' => $dataCagarBudaya['longitude'],
            'latitude' => $dataCagarBudaya['latitude'],
            'alamat' => $dataCagarBudaya['alamat'],
            'status' => $dataCagarBudaya['status']
        );
        $this->db->insert('cagar_budaya', $val);
        return $this->db->insert_id();

    }

    public function updateCagarBudaya($dataCagarBudaya, $id_cagar)
    {
        $val = array(
            'nama_cagar' => $dataCagarBudaya['nama_cagar'],
            'deskripsi' => $dataCagarBudaya['deskripsi'],
            'tahun_peninggalan' => $dataCagarBudaya['tahun_peninggalan'],
            'nama_jenis' => $dataCagarBudaya['nama_jenis'],
            'longitude' => $dataCagarBudaya['longitude'],
            'latitude' => $dataCagarBudaya['latitude'],
            'alamat' => $dataCagarBudaya['alamat'],
            'status' => $dataCagarBudaya['status']
        );
        $this->db->where('id_cagar', $id_cagar);
        $this->db->update('cagar_budaya', $val);
    }


    public function updateCagarStatus($dataCagarBudaya, $id_cagar){
      $val = array(
          'status' => $dataCagarBudaya['status']
      );
      $this->db->where('id_cagar', $id_cagar);
      $this->db->update('cagar_budaya', $val);
    }
    public function deleteCagarBudaya($id_cagar)
    {
        $val = array(
            'id_cagar' => $id_cagar
        );
        $this->db->delete('cagar_budaya', $val);
    }

}
