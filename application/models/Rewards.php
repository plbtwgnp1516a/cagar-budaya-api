<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rewards extends CI_Model {

    public function getCountRewards()
    {
        return $this->db->count_all_results('rewards', FALSE);
    }

    public function getRewards($page, $size)
    {
        return $this->db->get('rewards', $size, $page);
    }

    public function insertRewards($dataRewards)
    {
        $val = array(
            'nama_reward' => $dataRewards['nama_reward'],
            'deskripsi' => $dataRewards['deskripsi'],
            'minimal_poin' => $dataRewards['minimal_poin']
        );
        $this->db->insert('rewards', $val);
    }

    public function updateRewards($dataRewards, $id_reward)
    {
        $val = array(
            'nama_reward' => $dataRewards['nama_reward'],
            'deskripsi' => $dataRewards['deskripsi'],
            'minimal_poin' => $dataRewards['minimal_poin']
        );
        $this->db->where('id_reward', $id_reward);
        $this->db->update('rewards', $val);
    }

    public function deleteRewards($id_reward)
    {
        $val = array(
            'id_reward' => $id_reward
        );
        $this->db->delete('rewards', $val);
    }

}