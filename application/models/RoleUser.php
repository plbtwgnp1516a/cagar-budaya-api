<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RoleUser extends CI_Model {

  public function getidRole($nama_role)
  {
    $this->db->select('id_role');
    $this->db->from('role');
    $this->db->where('nama_role', $nama_role);
    $data = $this->db->get()->row()->id_role;
    return $data;
  }
}
